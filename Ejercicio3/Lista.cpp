#include <iostream>
#include <stdio.h>
#include <string.h>

//Definición de la clase
#include "Lista.h"

using namespace std;

Lista::Lista(){

}

void Lista::crear_nodo(char *nombre){
    Nodo *temporal = new Nodo;

    // Se liga al nodo siguiente, creando el espacio de memoria
    for (int i = 0; i < strlen(nombre); i++){
        temporal->dato[i] = nombre[i];
    }

    temporal->nodo_sig = NULL;

    // Si el es primer nodo de la lista, lo deja como raíz y como último nodo
    if (this->inicio == NULL) { 
        this->inicio = temporal;
        this->ultimo = this->inicio;
    } 

    // Si no apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    else {
        this->ultimo->nodo_sig = temporal;
        this->ultimo = temporal;
    }

   //ordenar_lista(this->inicio); 
}
 
/*void Lista::ordenar_lista(Nodo *temporal){
    int aux;

    while(temporal != NULL){
        Nodo *temporal2 = temporal->nodo_sig;
            while(temporal2 != NULL){
                if(temporal->dato > temporal2->dato){
                    aux = temporal2->dato;
                    temporal2->dato = temporal->dato;
                    temporal->dato = aux;
                }
            temporal2 = temporal2->nodo_sig;
        }
    temporal = temporal->nodo_sig;
    }

}*/

void Lista::imprimir(){
    Nodo *temporal = this->inicio;

    while (temporal != NULL) {
        cout << " - " << temporal->dato << endl; 
        temporal = temporal->nodo_sig;
    }
}

