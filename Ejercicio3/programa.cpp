#include <iostream>
#include "Lista.h"
//#include "Alacena.h"

using namespace std;

void agregar_postres(Lista *lista){
    int salir = 1;
    int tam;
    
//    Alacena ingredientes = Alacena();

    while(salir == 1){
        cout << "Ingrese el tamaño del nombre del postre: ";
        cin >> tam;
        
        char nombre_postre[tam];
        nombre_postre[tam] = {'\0'};

        cout << "Ingrese el nombre del postre:  ";
        for (int i = 0; i < tam; i++){
            cin >> nombre_postre[i];
        }

        system("clear");
        
  //      ingredientes.agregar_ingredientes();

        // Se crea la lista y se agregan los elementos
        lista->crear_nodo(nombre_postre);

        cout << "\n\n [1] Seguir ingresando postres\n [2] Salir" << endl;
        cin >> salir;

    }
}
void menu(Lista *lista){
    int opcion;
    
    cout << "\nMENU\n" << endl;
    cout << " [1] Agregar postres\n [2] Mostrar postres" << endl;
    cout << " [3] Eliminar postres\n [4] Seleccionar postre\n [5] Salir" << endl;
     
    cout << "\n Ingrese su opción: ";
    cin >> opcion;
    
    system("clear");

    while(getchar() != '\n');
    
    switch(opcion){
        case 1:
            agregar_postres(lista); 
            system("clear");
            menu(lista);
            break;

        case 2:
            cout << "\nLOS POSTRES SON: " << endl;
            lista->imprimir();
            menu(lista);
            break;

        case 5:
            break;
    }

}
int main(){
    // Crear lista
    Lista *lista =  new Lista();

    cout << "\n\tREPOSTERIA ICB" << endl;
    menu(lista);
     

    return 0;
}
