#include <iostream>
#include "Lista.h"

using namespace std;

void dividir_lista(Lista *lista, Lista *lista_negativa, Lista *lista_positiva){
    int negativo;
    int positivo;
    
    Nodo *temporal = new Nodo();
    temporal = lista->get_inicio();

    while(temporal != NULL){
        if(temporal->numero < 0){
            negativo = temporal->numero;
            lista_negativa->crear_nodo(negativo);
        }
    
        else{
            positivo = temporal->numero;
            lista_positiva->crear_nodo(positivo);
        }

        temporal =  temporal->nodo_sig;
    }

}

int main(){
    // Crear lista
    Lista *lista =  new Lista();
    Lista *lista_negativa =  new Lista();
    Lista *lista_positiva =  new Lista();

    int salir = 1;
    int numero;

    cout << "\n\tLISTA ENLAZADA" << endl;

    while(salir == 1){
        cout << "\nIngrese un número, puede ser negativo o positivo" << endl;
        cout << " --> ";
        cin >> numero;
        
        system("clear");

        // Se crea la lista y se agregan los elementos
        lista->crear_nodo(numero);

        cout << "\n\n [1] Seguir ingresando números\n [2] Salir" << endl;
        cin >> salir;
    }
    
    dividir_lista(lista, lista_negativa, lista_positiva);

    system("clear");

    cout << "\n\tLISTA DE NÚMEROS NEGATIVOS" << endl;
    lista_negativa->imprimir();

    cout << "\n\tLISTA DE NÚMEROS POSITIVOS" << endl;
    lista_positiva->imprimir();

    return 0;
}
