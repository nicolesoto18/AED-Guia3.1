#include <iostream>
#include <stdio.h>
#include <string.h>

//Definición de la clase
#include "Lista.h"

using namespace std;

Lista::Lista(){

}

void Lista::crear_nodo(char *nombre){
    Nodo *temporal = new Nodo;

    // Se liga al nodo siguiente, creando el espacio de memoria
    for (int i = 0; i < strlen(nombre); i++){
        temporal->dato[i] = nombre[i];
    }

    temporal->nodo_sig = NULL;

    // Si el es primer nodo de la lista, lo deja como raíz y como último nodo
    if (this->inicio == NULL) { 
        this->inicio = temporal;
        this->ultimo = this->inicio;
    } 

    // Si no apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    else {
        this->ultimo->nodo_sig = temporal;
        this->ultimo = temporal;
    }
}

void Lista::imprimir(){
    Nodo *temporal = this->inicio;

    cout << "\tLISTA DE NOMBRES" << endl;

    while (temporal != NULL) {
        cout << temporal->dato << " - ";
        temporal = temporal->nodo_sig;
    }
}

