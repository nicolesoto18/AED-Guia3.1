#include <iostream>
#include "Lista.h"

using namespace std;

int main(){
    // Crear lista
    int salir = 1;
    int tam;
    Lista *lista =  new Lista();

    cout << "\n\tLISTA ENLAZADA" << endl;

     
    while(salir == 1){
        cout << "Ingrese el tamaño del nombre: ";
        cin >> tam;
        
        char nombre[tam];
        nombre[tam] = {'\0'};

        cout << "Ingrese el nombre:  ";
        for (int i = 0; i < tam; i++){
            cin >> nombre[i];
        }

        system("clear");

        // Se crea la lista y se agregan los elementos
        lista->crear_nodo(nombre);

        lista->imprimir();

        cout << "\n\n [1] Seguir ingresando nombres\n [2] Salir" << endl;
        cin >> salir;

    }

    return 0;
}
